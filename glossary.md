# Glossary for keys.openpgp.org bootstrap committee

The keys.openpgp.org bootstrap committee uses the following
terminology specific to its work.

* board---a future organization to oversee and guide the
  keys.openpgp.org service so it serves well the OpenPGP ecosystem
* bootstrap committee---the group preparing a proposal for how the
  board will operate
* Hagrid---the verifying key server that implements the
  keys.openpgp.org service
  - see <https://gitlab.com/hagrid-keyserver/hagrid>
* KEP---keys.openpgp.org enhancement proposal
* koo---the keys.openpgp.org site and service
