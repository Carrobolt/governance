# KOO Board Meeting 2023-09-29

## Attendees
- Ola
- Neal
- Daniel
- Lukas

Absent without excuse:
- Vincent

## Pass along maintenance and development of KOO
- Devan appears interested (dvn <mail@dvn.me>, 0CC011A7C3DC6EB475679BC9AC0ABA31866A7E76) in doing operations and is very experienced
- Vincent should reach out to him to discuss detail

## OpenPGP Working Group Update regarding HKP and V4/V6 keys
- Daniel proposed a new charter that did include HKP
  https://gitlab.com/openpgp-wg/openpgp-wg-admin/-/blob/main/charter.md
  (Not accepted yet, but current version being discussed)
- Rather large list – current task is to order them by priority
- Neal: We should wait to see what HKP does in regards to handling V4/V6 keys then
- Daniel: Would you like to make HKP the standardardized API and then retire the old API (of KOO)
- Neal: Actually, yeah
- Daniel: If we wait for RFC that might take a long time
- Neal: If we are going to invest energy, then we may as well work with others in order to get more feedback.
- Neal: There are two separate issues, how to retrieve information and how to submit information. If we could have the same API for retrieving information that would already go a long way.  Let's first try to work with the other implementations and only if that fails do we go our own way.
- Action: Reach out to AndrewG and ask if he is intersted in collaborating and how, and if there is anyone else interested in this topic.  Neal will send an email to Andrew and cc the board.
- 

## Voting Process KOO Board
- Lukas reached out to Patrick and dkg about running / counting the votes for the next election. Patrick said he would do it if dkg is okay with the process.  dkg has not yet replied.
- Neal: shouldn't postpone it for too long
- Daniel: We should discuss the timeline with Patrick and dkg
- Neal: We should have a decision in the next meeting and should ideally start the self-nomination period very shortly after
- If dkg doesn't respond we should think about alternative candidates or Lukas will run it


  
- Content of the original email to dkg
```
Hi Daniel,

I hope everything is fine!

In the last KOO board meeting we came to the conclusion that we would prefer for the election not to be held by me – I would be designated to hold it as secretary of the board – but instead to „out-source“ it to Patrick to avoid any kind of conflict of interest.

In addition, since there’s been some criticism after the last election regarding the election procedure, as anyone could see how voted for whom, we would like to hold a semi-private election, but without introducing additional tooling in order to keep it as simple as possible.
So the idea is two delegate the election counting to two trusted third-parties – we were thinking of you and Patrick – who would then only publish the final result and not the single votes as last year.

Would that be an option for you?

Thank you and looking forward to hearing from you.

Cheers,

Lukas
```

## Short Introduction of Devan
- Works with Neal on the Sequoia team
- System administration since the last 15 years
- In the last 5-8 years specifically working on CI
- Worked with PeP, on SecuShare, ...
- Has been advocating to use KOO
- KOO board introduces themselves
...
- Neal: KOO is hosted at Greenhost.nl in the Netherlands
- Neal: Only Vincent has currently access to it
- Neal: Couple of outages in the last couple of years
- Neal: Main work would be to watch and do devops (reboot if necessary, install necessary updates, work on backup solution, ...)
- Neal: Whoever takes over from Vincent may want to be part of the board and nominate themselves
- Daniel: It's not necessary that the ops person must be a board member
- Ola: It's kind of orthogonal. Ops person could be board member, but most important is that there would be semi-regular meetings with the board.
- Devan: Joining the board should be a later discussion if at all and would first mainly get devops running
- Neal: There should be a long-term interest since the handover could take quite some time
- Daniel: Doesn't have to be a 
- Devan: Even if there's not a long onboarding period it might take a bit of time to learn how everything works
- Lukas: It would be great to have some documentation as a first step so that if dvn loses interest or has to leave, then it will be easier for one of us to take over or someone else.
- Lukas: Lukas remembers that Vincent said that there was no ops documentation.
- Devan: It would be great to get an understanding of how many hours have to be put into it.
- Neal: From what we known from Vincent it's not too much time at all. Maybe a few hours every few months. The other thing would be how much time Devan would like to put into it and what tasks he would like to take on.

Decision about Devan: Unanimous acceptance

## Operations Team
- Daniel: Operations Team and Board should be separate. Also since the board can fire the operations team
- Neal: Not sure if there's enough interest to support a Board *and* a Operations Team
- Daniel: There could be a possibility that the devs add something to KOO which the community doesn't like and then the board should have the possibility to fire the devs
- Ola: Doesn't see why the ops and devs shouldn't be on the board but they don't necessarily need to be on the board


## Actions
- Daniel H.: will send dkg an mail.
- Neal: Reach out to AndrewG and ask if he is intersted in collaborating and how, and if there is anyone else interested in this topic.  Neal will send an email to Andrew and cc the board.
- Lukas: will send an email to Vincent to reach out to Devan and discuss handover introduction


## Agenda Next Meeting
- Discuss if board should have access to the KOO server

## Next Meeting October 13th
