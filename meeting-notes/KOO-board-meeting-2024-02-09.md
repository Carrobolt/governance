# KOO Board Meeting - 2024-02-09

Present: Daniel, Lukas, Ola, Vincent

## Agenda:

* Operational update
** updated hagrid deps
** regenerated link database to fix inconsistencies from issues in december
** filled in short survey for eclips.is
*** current costs (covered by OTF): ~150EUR
*** could downsize, but should at least wait until we have a feeling for proton usage
** using openssl now as crypto lib to support brainpool

* Update on Devan?
** didn't get to it yet, sorry :(
** but, made some steps preparing ansible & contacting eclips.is


* Update on website/Neighbourhoodie

Meeting notes from that meeting:
Meeting with [[Neighbourhoodie]] https://neighbourhood.ie/
with [[Alex Feyerke]]
Frontend lead, frontend dev
Neighbourhoodie got job from [[STF]] to do bug resilience improvements
Bug resilience in a broad sense: "do chores so we can focus on the important stuff", which makes working on websites a legitimate case
(Intros from Vincent and Daniel)
Vincent: rough overview of KOO, most interesting is restructuring of how /about pages are built
In particular, improve localization story
Daniel: on the visual side, we did make a logo, could do some updates
Using hugo would be ok
Project management: Open to accommodate, regular meetings fine, or could just do the work and come back later
Next steps:
Discuss how fits into project structure / capacity for neighbourhoodie
Get the go-ahead from STF (is it part of the existing web improvement project for sequoia, or a separate project?)

Daniel: thought if they could also work on the UX, related to the UX that is part of the KEP-2 proposal

* Interim on UX

Vincent: most users use KOO via client support (JSON + email verification), so website UX is not biggest concern
Daniel: might become more important with v6/pqc concerns

* Update on key lookups from Proton

Daniel: we finally implemented it 🚀
** will gradually roll it out to users, starting at 1% or so
** should watch KOO load during rollout

V: Current rate limit is 100r/s
D: might go over that.
V: we'll just adjust then

V: can the lookp be turned off flexibly?
D: yes, we can turn it off quickly
D: could also increase cache duration for a day to keep service functional

* Voting body membership list

D: wanted to add member, but was already there
D: compared to list of voters in gitlab repo (from 2023 election), noticed a few others not in there

found some inconsistencies

D: can check with dkg, might want to have a canonical source
V: do we just want to decide and do it?

vote: keep addresses in a gitlab repo
result: all in favor

* Question / slightly modified proposal about key signing

D: could we do SXG just for proton? undocumented and fairly tightly communicated
V: what about certificate management?
D: just use self-signed and we pin at proton
V: nginx module is unmaintained (last commit March 2021)
D: no better approach, but it's still fine for what it is

We can roll this out experimentally
Opinion tally: cautious "yes" from Lukas, Ola, and Vincent, provided on keeping it in a state where we can easily back out and don't strongly commit to maintaining it

* Access to list of voting body email addresses?

V: feels weird to have public, should be in private repo?
D: it's already sort of public from the public keys

[discussion on list of voting body email addresses]

rough conclusion: should be private, let's take steps to make it so

* Discussion on privacy-preserving lookups, e.g. OHTTP (perhaps only if we have time?)

no time, we are 100 minutes into this meeting

## Actions

[ ] Setup private repo to collect the email addresses of the voting body as canonical source of truth
[ ] Daniel: ask dkg about subscribed people not in the GitLab repo
[ ] Vincent: Check out SXG module
[ ] Daniel: update KEP-3
