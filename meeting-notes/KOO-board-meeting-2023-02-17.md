Present: Neal, Daniel, Lukas, Ola, Vincent
Absent:
Absent with excuses:

# Agenda

* Discuss action items from previous meeting and their progress
* Discuss process for accepting members
* Operations team report
* Continue on KOO backlog items


# Action Items from previous meeting

- [ ] Lukas: look into available CoC's and talk to people who might have more experience
- [ ] Vincent: send out voting body invites
- [ ] Neal: send invites to the individual members we would like to add to the voting body
- [ ] Vincent: (possibly as part of onboarding operations team member?) document the single components of the current server infrastructure and tasks the operations team is – and future members would be – responsible for.
- [ ] Write introductory item about the Board on KOO
  - At the openpgp email summit decided to move to a community-based governance model
  - First election was in Nov
  - New board consisting of xxx first convened in Dec
  - Looking for people to join the election body (link to Vincent's post)
  - The server is running nicely without major issues

  - Vincent on tone:
    - wants the blog to be approachable to end users (not technical; no talk about specs)
    - Emoji or two at the end, but not all over


# Discuss formal process for accepting members

* Formulate steps necessary to be accepted as new member
* Specific email for contacting the board members: board@keys.openpgp.org
    - board@ certificate
    - Operations team has the primary key
    - Only a single encryption capable subkey (no signing capable subkey)
    - Subkey expires at end of term
    - New encryption capable subkey with each board
    - op team provides revocation cert to board members
* Provide template for informal email to join the voting body. Highlight affiliations
* Lukas will send initial response to emails to board@keys.openpgp.org that will be discussed in the next board meeting


# Operations team report

* Fixed a bug in the management interface that affected users of some languages
* Added note to README about the maintenance status of hagrid
* Received a bug report about angle brackets not handled correctly in email addresses. We will not allow angle brackets in user ids. Causes all sorts of headaches


# Continue on KOO Backlog items

## Rate Limiting

* Rate limit increases can be requested via board@keys.openpgp.org

## Handling of feature requests

* Write a KOO Enhancement Proposal
* Board will discuss based on that


# Action Items

- [ ] Vincent: Setup board@keys.openpgp.org and forwarding to all board members
- [ ] Vincent creates board key, distributes it to board members with revocation cert
- [ ] Neal: Provide script to setup key pair
- [ ] Lukas: look into available CoC's and talk to people who might have more experience

## Blocked by member acceptance process

- [ ] Vincent: send out voting body invites
- [ ] Neal: send invites to the individual members we would like to add to the voting body
- [ ] Vincent: (possibly as part of onboarding operations team member?) document the single components of the current server infrastructure and tasks the operations team is – and future members would be – responsible for.
- [ ] Write introductory item about the Board on KOO
  - At the openpgp email summit decided to move to a community-based governance model
  - First election was in Nov
  - New board consisting of xxx first convened in Dec
  - Looking for people to join the election body (link to Vincent's post)
  - The server is running nicely without major issues

  - Vincent on tone:
    - wants the blog to be approachable to end users (not technical; no talk about specs)
    - Emoji or two at the end, but not all over
- [ ] Create MR for the public facing documentation of the process how to become a member of the voting body
- [ ] Lukas: add "Operation teams report" as default agenda point
- [ ] Daniel: Formulate the process how to request a rate limit increase
- [ ] Daniel: Write a KAP for KOO signing User IDs


# Agenda for next meeting March 10th

* Discuss process of how to write a handle KAPs (KOO Enhancement Request)

