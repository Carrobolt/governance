Present: Neal, Daniel, Lukas, Ola, Vincent
Absent:
Absent with excuses:

# Agenda

## Introductions: Who we are and our personal goals for KOO

## Agenda
* Decide on a secretary
* Decide who will have an initial 2 term limit
* Decide how we will communicate and collaborate
* Transparency: how much of our meeting notes will be made available and where?
* Talk about the process for adding new members

# Decide on a secretary
* Main Secretary: Lukas Pitschl
* Backup Secretary: Ola Bini

Backup Secretary will help in organizing meetings. Final reminder will be sent out by Main Secretary one week before the meeting.


# Meeting Frequency
* Every 2 weeks for start, Friday 3pm CET.
* One more meeting this year.


# Decide who will serve 2 year terms at most
* Ola Bini
* Neal H. Walfield


#  Decide how we will communicate and collaborate
* Email (all board members cc'd) - encrypted and signed
* Meetings: video conference via pep.security


# Transparency: how much of our meeting notes will be made available and where?
* Notes will be published on Governance repository
* Notes will also be sent to the KOO mailing list
* By default, all notes will be published.
* Redaction of private information within notes will be decided case by case.
* Notes will be formatted with Markdown


# Talk about the process for adding new members (voting body)
* By default everyone can request to be added and should be added unless there are serious objections raised. E.g. an organization appears to be trying to take over the org
* Don't have to be a developer or related to a project: OpenPGP users are fine, if they care about KOO


# How to handle new features for Hagrid
* For not trivial feature requests a formal Enhancement Proposal should be written and the board will discuss whether to accept it or not.


# Upcoming Meeting Agenda
* Remain on Gitlab or switch to alternative like Codeberg or Github - it's unclear what Gitlab is planning for free plans.
* Discuss the process for adding members to the Operations team
* Decide if a code of conduct should be used when communicating via public channels (mailing list, repository)
* Talk about how to proactively invite new members which were not previously fitting the criteria.
* Discuss open merge requests / formal enhancement proposals
* Authenticate fingerprints of board members


# Action Items
- [ ] Prepare technical backlogs to discuss in next meeting (Vincent)
- [ ] Overview of what has happened on the operations side (Vincent)


# Fingerprint exchange

Neal:
```    
pub   rsa3744 2015-04-07 [SC] [expires: 2023-04-08]
      8F17 7771 18A3 3DDA 9BA4  8E62 AACB 3243 6300 52D9
uid           [ultimate] Neal H. Walfield <neal@walfield.org>
uid           [ultimate] Neal H. Walfield <neal@gnupg.org>
uid           [ultimate] Neal H. Walfield <neal@pep-project.org>
uid           [ultimate] Neal H. Walfield <neal@pep.foundation>
uid           [ultimate] Neal H. Walfield <neal@sequoia-pgp.org>
```

Ola:
```
Gitlab.com username: olabiniV2
    
pub   ed25519/0x6786A150F6A2B28F 2019-07-08 [SC] [expires: 2024-04-12]
      Key fingerprint = 7B45 78C7 D012 2BF7 6B5E  E5DD 6786 A150 F6A2 B28F
uid                   [ultimate] Ola Bini <ola@olabini.se>
sub   cv25519/0x0DFC26896428ADF9 2019-07-08 [E] [expires: 2024-07-10]
```

Daniel:
```
GitLab.com username: twisstle

pub   ed25519 2022-08-18 [SC]
      72E33AE81300E553BC4EEDEFCB064A128FA90686
uid           [ultimate] d.huigens@protonmail.com <d.huigens@protonmail.com>
sub   cv25519 2022-08-18 [E]
```

Lukas:

```
Gitlab.com username: luke_le

pub   dsa2048 2010-07-29 [SC] [verfällt: 2024-06-07]
      608B 00AB E1DA A350 1C5F  F91A E582 7132 6F9F 4937
uid        [ ultimativ ] Lukas Pitschl <lukele@gpgtools.org>
sub   rsa4096 2017-08-02 [E] [verfällt: 2023-08-04]
```

Vincent:
```
pub   rsa4096 2012-07-14 [SC]
      D4AB192964F76A7F8F8A9B357BD18320DEADFA11
uid           [ultimate] Vincent Breitmoser <look@my.amazin.horse>
sub   rsa4096 2014-10-13 [E]
```