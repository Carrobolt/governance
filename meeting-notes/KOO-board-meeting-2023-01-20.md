Present: Neal, Daniel, Lukas, Ola, Vincent
Absent:
Absent with excuses:

# Agenda

* Discuss action items from previous meeting and their progress
* Discuss voting mechanism for the next election
* Discuss KOO backlog items


# Action Items from previous meeting

- [x] Vincent: reach out to potential candidate who has expressed interest in joining operations team
- [x] Vincent: Create the backlog of current issues
- [ ] Lukas: look into available CoC's and talk to people who might have more experience
- [ ] Vincent: send out voting body invites
- [ ] Neal: send invites to the individual members we would like to add to the voting body
- [ ] Vincent: (possibly as part of onboarding operations team member?) document the single components of the current server infrastructure and tasks the operations team is – and future members would be – responsible for.

## Reach out to potential candidate for operations team
* Potential candidate is still interested. Very busy right now. Will look into it in June.
* Vincent will reach out in June again.

# Voting mechanism for the next election
* Public voting introduced some hesitance
* Think about a more private solution. Might require either technical solution or trusted party.

# KOO Backlog items

## Discuss privacy policy

* Briefly received a general overview of the current privacy policy of KOO

## Raised rate limit process

https://keys.openpgp.org/about/api#rate-limiting

* KOO has rate limits in place, fairly loose for key lookups, fairly strict (with large burst) for email lookups
* We currently have an increased rate limit for flowcrypt
* Proton (Daniel) asked about this, we could also increase for them
* Should we have a process for this? Either way, I left it alone when the board was on the horizon


# Action Items
* [ ] Lukas: look into available CoC's and talk to people who might have more experience
* [ ] Vincent: send out voting body invites
* [ ] Neal: send invites to the individual members we would like to add to the voting body
* [ ] Vincent: (possibly as part of onboarding operations team member?) document the single components of the current server infrastructure and tasks the operations team is – and future members would be – responsible for.
- [ ] Write introductory item about the Board on KOO
  - At the openpgp email summit decided to move to a community-based governance model
  - First election was in Nov
  - New board consisting of xxx first convened in Dec
  - Looking for people to join the election body (link to Vincent's post)
  - The server is running nicely without major issues
  
  - Vincent on tone:
    - wants the blog to be approachable to end users (not technical; no talk about specs)
    - Emoji or two at the end, but not all over
